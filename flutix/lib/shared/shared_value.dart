part of 'shared.dart';

String apiKey = '5aa32a755442f89519ee3a3ee358e71a';
String imageBaseUrl = 'https://image.tmdb.org/t/p/';

PageEvent prevPageEvent;
//2b karena status dari firebase auth akan diberikan oleh si firebase auth ke dalam aplikasi kita
//secara otomatis, jadi kita gak bisa mengontrol kapan dia kasih/gak kasih
//jadi kadang ada beberapa kasus ketika si firebase auth memberikan lebih dari satu kali di state yg sama
//nah itu bisa membuat masuk ke page yg sama dua kali
//maka dari itu kita harus mencatat dari page sebelumnya, supaya tidak melakukan perpindahan page seperti diatas ini

File imageFileToUpload;

//4k kenapa string karena setelah di uload karena ingin mengembalikan download url dari si file gamber
Future<String> uploadImage(File image) async {
  //4k mengambil nama file
  //basename ada di path.dart
  String fileName = basename(image.path);
  //4k akan diarahkan ke firebaseStorage dgn namanya si namefile ini
  StorageReference ref = FirebaseStorage.instance.ref().child(fileName);
  //4k memberikan tugas kpd reference untuk mengupload si file nya
  StorageUploadTask task = ref.putFile(image);
  //4k lalu meminta kembalian nya
  StorageTaskSnapshot snapshot = await task
      .onComplete; //jadi ketika dia komplit akan mengembalikan storagetasksnapshot

  //4k setlah dapat StorageTaskSnapshot buat downloadUrl nya
  return await snapshot.ref.getDownloadURL();
}
