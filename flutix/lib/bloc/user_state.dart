part of 'user_bloc.dart';

abstract class UserState extends Equatable {}

class UserInitial extends UserState {
  @override
  List<Object> get props => [];
}

//2c UserLoaded dimana data user sudah ke load sama kita
//dan disni User menggunakan model
//jadi blocstate itu dimasukan ke UI
//sedagnkan blocevent itu dari UI ke bloc
class UserLoaded extends UserState {
  final User user;
  UserLoaded(this.user);

  @override
  List<Object> get props => [user];
}
