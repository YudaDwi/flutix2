import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutix/models/models.dart';
import 'package:flutix/services/services.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  // UserBloc() : super(UserInitial());

  @override
  Stream<UserState> mapEventToState(
    UserEvent event,
  ) async* {
    if (event is LoadUser) {
      User user = await UserServices.getUser(event.id);
      yield UserLoaded(user);
    } else if (event is SignOut) {
      yield UserInitial();
      //2c kalau event adl SignOut maka
      //akan mengembalikan UserInitial/kembali ke awal state
    } else if (event is UpdateData) {
      //4k kalau event adl updatedata maka
      //updatedUser yaitu user terbaru yg sudah update
      //state saat ini sbg UserLoaded
      User updatedUser = (state as UserLoaded)
          .user
          .copyWith(name: event.name, profilePicture: event.profileImage);

      //5a kurang update database
      await UserServices.updateUser(updatedUser);
      yield UserLoaded(updatedUser);
    }
  }

  @override
  UserState get initialState => UserInitial();
}
