part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();
}

//2c yg dibutuhkan ketika LoadUser itu adl id user itu sendiri
class LoadUser extends UserEvent {
  final String id;
  LoadUser(this.id);

  @override
  List<Object> get props => [id];
}

//2c untuk signout memang tidak menambahkan field apapun
class SignOut extends UserEvent {
  @override
  List<Object> get props => [];
}

//4k buat update user event
//dia minta nama/profile image karena yg bisa dirubah cuma ke dua nya itu
class UpdateData extends UserEvent {
  final String name;
  final String profileImage;

  UpdateData({this.name, this.profileImage});
  @override
  List<Object> get props => [name, profileImage];
}
