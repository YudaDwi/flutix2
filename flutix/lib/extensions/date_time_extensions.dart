part of 'extensions.dart';

extension DateTimeExtension on DateTime {
  String get dateAndTime => "${this.shortDayName} ${this.day}, ${this.hour}:00";
  String get shortDayName {
    //6c jadi weekday menunjukan bahwa hari ini senin atau selasa atau hari yg lain
    //karena return itu langsung keluar dari method  jadi tidak perlu menggunakan break
    switch (this.weekday) {
      case 1:
        return 'Mon';
      case 2:
        return 'Tue';
      case 3:
        return 'Wed';
      case 4:
        return 'Thu';
      case 5:
        return 'Fri';
      case 6:
        return 'Sat';
      default:
        return 'Sun';
    }
  }
}
