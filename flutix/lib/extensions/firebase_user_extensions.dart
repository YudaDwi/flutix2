part of 'extensions.dart';

extension FirebaseUserExtension on FirebaseUser {
  User convertToUser(
          {String name = 'no name',
          List<String> selectedGenres = const [],
          String selectedLanguange = 'English',
          int balance = 100000}) =>
      User(this.uid, this.email,
          name: name,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguange,
          balance: balance);

  //1f ini extension berfungsi untuk mengambil data dari firestore
  //dan yg diambil adalah data uid/nomor dari id tsb
  //dan uid itu sudh bawaan dari flutter framework
  Future<User> fromFirestore() async => await UserServices.getUser(this.uid);
}
//jadi extension itu adalah kita dapat menambahkan method/properti tambhan pd class lain yg bukan dibuat oleh kita
//1d jadi setelah login data akan masuk ke FirebaseUser/cloud
//nah dari firebaseUser akan dibuatkan extension yg mereturn ke arah objek User/models
//krena profile picture bisa ada/tidak maka di models/user dibuat opsional
