part of 'services.dart';

class AuthServices {
  static FirebaseAuth _auth = FirebaseAuth.instance;

  //1e tipe kembalian dirubah ke SignInSignUpResult
  //yg nantinya akan memberi message dan user
  static Future<SignInSignUpResult> signUp(String email, String password,
      String name, List<String> selectedGenres, String selectedLanguage) async {
    try {
      AuthResult result = await _auth.createUserWithEmailAndPassword(
          email: email, password: password);
      //jadi ini diconvert dari User ke user(firebaseUser)
      User user = result.user.convertToUser(
          //1d
          name: name,
          selectedGenres: selectedGenres,
          selectedLanguange: selectedLanguage);

      await UserServices.updateUser(user);
      return SignInSignUpResult(user: user);
      //1e jadi yg dikemblikan user class SignIn adalah user dari objek firebaseUserdan convert user
    } catch (e) {
      return SignInSignUpResult(message: e.toString());
    }
  }

  static Future<SignInSignUpResult> signIn(
      String email, String password) async {
    try {
      AuthResult result = await _auth.signInWithEmailAndPassword(
          email: email, password: password);
      //1f kalau berhasil buat User user yg berasal dari firebaseUser di authResult
      //maka dari itu kita butuh extension untuk mengubh si FirebaseUser menjadi user
      User user = await result.user.fromFirestore();
      return SignInSignUpResult(user: user);
    } catch (e) {
      return SignInSignUpResult(message: e.toString().split(',')[1].trim());
      //1f di split/dipotong lalu dipisahkan koma lalu diurutkan nomer 1
    }
  }

  static Future<void> signOut() async {
    await _auth.signOut();
  }

  //2a membuat stream dgn tipe kembalian FirebaseUser, untuk menyambungkan aplikasi kita dgn FirebaseAuth
  //supaya ada notif bilamana si pengguna itu signIn/signOut/signUp
  //onAuthStateChanged itu akan memberitahukan bila pengguna keluar/masuk/daftar
  static Stream<FirebaseUser> get userStream => _auth.onAuthStateChanged;
}
//kalau email/password berhasil maka akan disimpan ke cloud firestore
//nah data tadi akan menyimpan email-pswrd-name-selecgenre-selectlang
//maka dari itu dibuat user models

class SignInSignUpResult {
  final User user;
  final String message;

  SignInSignUpResult({this.user, this.message});
}
