part of 'services.dart';

//1e tempat untuk menyimpan data di cloud firestore
//kalau auth_services tempat untuk men set autentifikasi

class UserServices {
  static CollectionReference _userCollection =
      Firestore.instance.collection('users');
  //1e ini adl key pd firestore

  static Future<void> updateUser(User user) async {
    // String genres = '';

    // for (var genre in user.selectedGenres) {
    //   genres += genre + ((genre != user.selectedGenres.last) ? ', ' : '');
    // }

    //1e akan disimpan di cloud firestore
    //data ini yg akan dibutuhkan ketika sudah masuk ke aplikasi
    _userCollection.document(user.id).setData({
      'email': user.email,
      'name': user.name,
      'balance': user.balance,
      'selectedGenres': user.selectedGenres,
      'profilePicture': user.profiilePicture ?? '',
      'selectedLanguage': user.selectedLanguage
      //bila ada profile picture bila tidak ada maka string kosong
      //profilePicture di cek bila tidak ada maka diisi string kosong
    });
  }

  //1f kalau tidak salah fungsi ini untuk mengambil data dari yg sudah disimpan di atas
  static Future<User> getUser(String id) async {
    DocumentSnapshot snapshot = await _userCollection.document(id).get();

    return User(id, snapshot.data['email'],
        balance: snapshot.data['balance'],
        profiilePicture: snapshot.data['profilePicture'],
        //1f selected genres memang sebuah list maka dari itu dibuat list lalu dibuat mapping
        selectedGenres: (snapshot.data['selectedGenres'] as List)
            .map((e) => e.toString())
            .toList(),
        selectedLanguage: snapshot.data['selectedLanguage'],
        name: snapshot.data['name']);
  }
}
