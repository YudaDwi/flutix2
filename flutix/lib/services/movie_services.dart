part of 'services.dart';

class MovieServices {
  //parameter int page itu sesuai dgn apa yang diambil dan yg diambil adalah page pertama dari api tsb
  static Future<List<Movie>> getMovies(int page, {http.Client client}) async {
    String url =
        'https://api.themoviedb.org/3/discover/movie?api_key=$apiKey&language=en-US&sort_by=popularity.desc&include_adult=false&include_video=false&page=$page';
    //5a page nya yg akhir dimasukan parameter
    client ??= http.Client();
    //5a client di cek , kalau dia null maka akan diisi httpClient

    var response = await client.get(url);
    //salah satu method pemanggilan api dan ini men gate url

    if (response.statusCode != 200) {
      return [];
      //bila response status code tidak sama dgn  200/gagal maka akan me return list ksong
    }

    var data = json.decode(response.body);
    //5a bila response berhasil maka

    List result = data['results'];
    //5a string results itu diambil dari struktur api nya

    return result.map((e) => Movie.fromJson(e)).toList();
  }

  static Future<MovieDetail> getDetails(Movie movie,
      {http.Client client}) async {
    String url =
        'https://api.themoviedb.org/3/movie/${movie.id}?api_key=$apiKey&language=en-US';
    client ??= http.Client();
    //6a client di cek, kalau dia null maka akan diisi httpClient

    var response = await client.get(url);
    //6a salah satu method pemanggilan api dan ini men gate url

    var data = json.decode(response.body);
    //6a bila response berhasil maka

    List genres = (data as Map<String, dynamic>)['genres'];
    //6a karena ini di movie detail berbentuk list dan didalam list
    //berbentuk map lalu key dan value
    String language;
    //6a ini merubah data karena key original_language adl fi
    switch ((data as Map<String, dynamic>)['original_language'].toString()) {
      case 'ja':
        language = 'Japanese';
        break;
      case 'id':
        language = 'Indonesia';
        break;
      case 'ko':
        language = 'Korean';
        break;
      case 'en':
        language = 'English';
        break;
    }
    return MovieDetail(movie,
        language: language,
        genres: genres
            .map((e) => (e as Map<String, dynamic>)['name'].toString())
            .toList());
    //6a jadi setiap element di dalam genres itu adl map juga
  }

  static Future<List<Credit>> getCredit(int movieID,
      {http.Client client}) async {
    String url =
        "https://api.themoviedb.org/3/movie/$movieID/credits?api_key=$apiKey";

    client ??= http.Client();
    //6a client di cek , kalau dia null maka akan diisi httpClient
    var response = await client.get(url);
    //6a salah satu method pemanggilan api dan ini men gate url
    var data = json.decode(response.body);
    //6a bila response berhasil maka

    return ((data as Map<String, dynamic>)['cast'] as List)
        .map((e) => Credit(
            name: (e as Map<String, dynamic>)['name'],
            profilePath: (e as Map<String, dynamic>)['profile_path']))
        .take(8)
        .toList();
    //6a menurut saya data cast itu di pindah bentuk ke map
    //dan jadi bentuk Credit(model) dan dibentuk map lagi dengan value name dan profile_path
    //name dan profile_path itu sudah sesusai dengan key dari api tsb
  }
}
