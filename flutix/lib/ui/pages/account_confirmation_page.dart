part of 'pages.dart';

class AccountConfirmationPage extends StatefulWidget {
  final RegistrationData registrationData;
  AccountConfirmationPage(this.registrationData);

  @override
  _AccountConfirmationPageState createState() =>
      _AccountConfirmationPageState();
}

class _AccountConfirmationPageState extends State<AccountConfirmationPage> {
  bool isSigningUp = false;
  //4j bila ingin sign up ada indikator nya
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          context
              .bloc<PageBloc>()
              .add(GoToPreferencePage(widget.registrationData));
          return;
        },
        child: Scaffold(
          body: Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ListView(
              children: [
                Column(
                  children: [
                    //4j bgian arrow dan text
                    Container(
                      margin: EdgeInsets.only(top: 20, bottom: 82),
                      height: 56,
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: GestureDetector(
                              onTap: () {
                                context.bloc<PageBloc>().add(GoToPreferencePage(
                                    widget.registrationData));
                              },
                              child: Icon(
                                Icons.arrow_back_ios,
                                color: Colors.black,
                              ),
                            ),
                          ),
                          Center(
                            child: Text(
                              'confirm\nnew account',
                              style: blackTextFont.copyWith(fontSize: 20),
                              textAlign: TextAlign.center,
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      height: 105,
                      width: 105,
                      margin: EdgeInsets.only(bottom: 20),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image:
                                  (widget.registrationData.profileImage == null)
                                      ? AssetImage('assets/user_pic.png')
                                      : FileImage(
                                          widget.registrationData.profileImage),
                              fit: BoxFit.cover)),
                    ),
                    Text(
                      'welcome',
                      style: blackTextFont.copyWith(
                          fontSize: 13, fontWeight: FontWeight.w300),
                    ),
                    Text(
                      widget.registrationData.name,
                      style: blackTextFont.copyWith(fontSize: 20),
                      textAlign: TextAlign.center,
                    ),
                    SizedBox(height: 110),
                    (isSigningUp)
                        ? SpinKitFadingCircle(
                            size: 45,
                            color: Color(0xff3e9d9d),
                          )
                        : SizedBox(
                            height: 45,
                            width: 250,
                            child: RaisedButton(
                                color: Color(0xff3e9d9d),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(8)),
                                child: Text(
                                  'Create my account',
                                  style: whiteTextFont.copyWith(fontSize: 16),
                                ),
                                onPressed: () async {
                                  setState(() {
                                    isSigningUp = true;
                                  });

                                  imageFileToUpload =
                                      widget.registrationData.profileImage;
                                  //4j belum buat fungsi dari imageFileToUpload

                                  SignInSignUpResult result =
                                      await AuthServices.signUp(
                                          widget.registrationData.email,
                                          widget.registrationData.password,
                                          widget.registrationData.name,
                                          widget
                                              .registrationData.selectedGenres,
                                          widget.registrationData.selectLang);

                                  if (result.user == null) {
                                    setState(() {
                                      isSigningUp = false;
                                    });

                                    Flushbar(
                                      duration: Duration(seconds: 4),
                                      flushbarPosition: FlushbarPosition.TOP,
                                      backgroundColor: Color(0xffff5c83),
                                      message: result.message,
                                    )..show(context);
                                  }
                                }),
                          )
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
