part of 'pages.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  TextEditingController emailController = TextEditingController();

  TextEditingController passwordController = TextEditingController();

  bool isEmailValid = false; //3c untuk mencatat apakah email valid atau tidak
  bool isPasswordValid =
      false; //3c untuk mencata apakah password valid atau tidak
  bool isSingingIn =
      false; //3c untuk mencatat apakah sedang melakukan proses sign in/tidak

  @override
  Widget build(BuildContext context) {
    context
        .bloc<ThemeBloc>()
        .add(ChangeTheme(ThemeData().copyWith(primaryColor: accentColor2)));
    return WillPopScope(
      onWillPop: () {
        context.bloc<PageBloc>().add(GoToSplashPage());
        return;
      },
      child: Scaffold(
        body: Container(
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 30,
                  ),
                  SizedBox(
                    height: 70,
                    child: Image.asset('assets/logo.png'),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 70, bottom: 40),
                    child: Text(
                      'Welcome Back\nExplorer',
                      style: blackTextFont.copyWith(fontSize: 20),
                    ),
                  ),
                  TextField(
                    onChanged: (text) {
                      setState(() {
                        isEmailValid = EmailValidator.validate(text);
                        //3d kalau text saat ini memenuhi syarat emailvalidator maka dia akan bernilai ture
                        //set state yaitu keadaan yg diatur dan hanya ada di statefullwidget
                      });
                    },
                    controller: emailController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'email address',
                        hintText: 'email address'),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    onChanged: (text) {
                      setState(() {
                        //3d isPassword akan true ketika panjang password sama dgn 6
                        isPasswordValid = text.length >= 6;
                      });
                    },
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'password mu lur',
                        hintText: 'passwordmu lur'),
                  ),
                  SizedBox(
                    height: 6,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        'forgot password ? ',
                        style: greyTextFont.copyWith(fontSize: 12),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          'Get Now',
                          style: purpleTextFont.copyWith(fontSize: 12),
                        ),
                      )
                    ],
                  ),
                  Center(
                    child: Container(
                        width: 50,
                        height: 50,
                        margin: EdgeInsets.only(top: 40, bottom: 30),
                        child: isSingingIn
                            ? SpinKitFadingCircle(
                                color: mainColor,
                              )
                            : FloatingActionButton(
                                child: Icon(
                                  Icons.arrow_forward_ios,
                                  color: isEmailValid && isPasswordValid
                                      ? Colors.white
                                      : Color(0xffbebebe),
                                ),
                                backgroundColor: isEmailValid && isPasswordValid
                                    ? mainColor
                                    : Color(0xffe4e4e4),
                                elevation: 0,
                                //3d kalau isEmailValid dan isPasswordValid sudah benar maka
                                //isSigningIn bernilai true
                                onPressed: isEmailValid && isPasswordValid
                                    ? () async {
                                        setState(() {
                                          isSingingIn = true;
                                        });

                                        //3d
                                        SignInSignUpResult result =
                                            await AuthServices.signIn(
                                                emailController.text,
                                                passwordController.text);

                                        //3d jika data user itu masih kosong/belum terdaftar maka
                                        //isSingingIn bernilai false
                                        if (result.user == null) {
                                          setState(() {
                                            isSingingIn = false;
                                          });

                                          //3d dan mengeluarkan notifikasi
                                          Flushbar(
                                            duration: Duration(seconds: 4),
                                            flushbarPosition:
                                                FlushbarPosition.TOP,
                                            backgroundColor: Color(0xffff5c83),
                                            message: result.message,
                                          )..show(context);
                                        }
                                      }
                                    : null)),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Start fresh now? ',
                        style: greyTextFont.copyWith(fontSize: 14),
                      ),
                      GestureDetector(
                        onTap: () {},
                        child: Text(
                          'Sign Up',
                          style: purpleTextFont.copyWith(fontSize: 14),
                        ),
                      )
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
