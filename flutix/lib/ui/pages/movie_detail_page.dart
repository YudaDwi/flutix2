part of 'pages.dart';

class MovieDetailPage extends StatelessWidget {
  final Movie movie;
  MovieDetailPage(this.movie);

  @override
  Widget build(BuildContext context) {
    //6b jadi dua variabel ini akan mengisi yg di builder
    MovieDetail movieDetail;
    List<Credit> credits;

    return WillPopScope(
      onWillPop: () async {
        context.bloc<PageBloc>().add(GoToMainPage());
        return;
      },
      child: Scaffold(
        //TODO search futureBuilder
        body: Stack(
          children: [
            Container(
              color: mainColor,
            ),
            SafeArea(
                child: Container(
              color: Colors.white,
            )),
            ListView(
              children: [
                FutureBuilder(
                    future: MovieServices.getDetails(movie),
                    builder: (_, snapshot) {
                      if (snapshot.hasData) {
                        movieDetail = snapshot.data;
                      }

                      return Column(
                        children: [
                          Stack(
                            children: [
                              //backdrop
                              Stack(
                                children: [
                                  Container(
                                    height: 270,
                                    decoration: BoxDecoration(
                                        image: DecorationImage(
                                            //6b backdropPath dicek kalau dia null maka akan mengembalikan movie.posterPath
                                            image: NetworkImage(imageBaseUrl +
                                                    'w1280' +
                                                    movie.backdropPath ??
                                                movie.posterPath),
                                            fit: BoxFit.cover)),
                                  ),
                                  //ini untuk gradient
                                  Container(
                                    height: 271,
                                    width: double.infinity,
                                    decoration: BoxDecoration(
                                        gradient: LinearGradient(
                                            begin: Alignment(0, 1),
                                            end: Alignment(0, 0.6),
                                            colors: [
                                          Colors.white,
                                          Colors.white.withOpacity(0)
                                        ])),
                                  )
                                ],
                              ),
                              //back icon
                              Container(
                                margin: EdgeInsets.only(
                                    top: 20, left: defaultMargin),
                                padding: EdgeInsets.all(1),
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(5),
                                    color: Colors.black.withOpacity(0.04)),
                                child: GestureDetector(
                                  onTap: () {
                                    context
                                        .bloc<PageBloc>()
                                        .add(GoToMainPage());
                                  },
                                  child: Icon(
                                    Icons.arrow_back_ios,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                          //text judul film
                          Container(
                            margin: EdgeInsets.fromLTRB(
                                defaultMargin, 16, defaultMargin, 6),
                            child: Text(
                              //6b kenapa pemanggilan objek movie krena title hanya ada dari model di sana
                              movie.title,
                              textAlign: TextAlign.center,
                              style: blackTextFont.copyWith(
                                fontSize: 24,
                              ),
                            ),
                          ),
                          (snapshot.hasData)
                              ? Text(
                                  //6b sedangkan genresAndLanguage berada di movieDetail (model)
                                  movieDetail.genresAndLanguage,
                                  style: greyTextFont.copyWith(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400),
                                )
                              : SizedBox(),
                          SizedBox(
                            height: 6,
                          ),

                          RatingStar(
                            voteAverage: movie.voteAverage,
                            alignment: MainAxisAlignment.center,
                          ),
                          Align(
                            alignment: Alignment.topLeft,
                            child: Container(
                              margin: EdgeInsets.only(
                                bottom: 12,
                                left: defaultMargin,
                              ),
                              child: Text(
                                'Cast & Crew',
                                style: blackTextFont.copyWith(fontSize: 14),
                              ),
                            ),
                          ),
                          //foto pmain film
                          FutureBuilder(
                              future: MovieServices.getCredit(movie.id),
                              builder: (_, snapshot) {
                                if (snapshot.hasData) {
                                  credits = snapshot.data;
                                  return SizedBox(
                                      height: 115,
                                      child: ListView.builder(
                                          scrollDirection: Axis.horizontal,
                                          itemCount: credits.length,
                                          itemBuilder: (_, index) {
                                            return Container(
                                                margin: EdgeInsets.only(
                                                    left: (index == 0)
                                                        ? defaultMargin
                                                        : 0,
                                                    right: (index ==
                                                            credits.length - 1)
                                                        ? defaultMargin
                                                        : 16),
                                                child:
                                                    CreditCard(credits[index]));
                                          }));
                                } else {
                                  return SizedBox(
                                    height: 50,
                                    child: SpinKitFadingCircle(
                                      color: accentColor1,
                                    ),
                                  );
                                }
                              }),
                          Container(
                              margin: EdgeInsets.fromLTRB(
                                  defaultMargin, 24, defaultMargin, 8),
                              child: Align(
                                alignment: Alignment.topLeft,
                                child: Text(
                                  'Stroyline',
                                  style: blackTextFont.copyWith(fontSize: 14),
                                ),
                              )),
                          //deskripsi
                          Container(
                            margin: EdgeInsets.fromLTRB(
                              defaultMargin,
                              0,
                              defaultMargin,
                              30,
                            ),
                            child: Text(
                              movie.overView,
                              style: greyTextFont.copyWith(
                                  fontSize: 14, fontWeight: FontWeight.w400),
                            ),
                          ),
                          RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8)),
                              color: mainColor,
                              child: Text(
                                'Continue To Book',
                                style: whiteTextFont.copyWith(fontSize: 16),
                              ),
                              onPressed: () {
                                context
                                    .bloc<PageBloc>()
                                    .add(GoToSelectSchedulePage(movieDetail));
                              }),
                          SizedBox(
                            height: defaultMargin,
                          )
                        ],
                      );
                    })
              ],
            )
          ],
        ),
      ),
    );
  }
}
