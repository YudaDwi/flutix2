part of 'pages.dart';

class PreferencePage extends StatefulWidget {
  final List<String> genres = [
    'horor',
    'music',
    'drama',
    'war',
    'crime',
    'action'
  ];
  final List<String> languages = ['indonesia', 'english', 'korea', 'japan'];

  //4h mungkin fungsi pindah/navigasi akan masuk lewat registration data
  final RegistrationData registrationData;
  PreferencePage(this.registrationData);
  @override
  _PreferencePageState createState() => _PreferencePageState();
}

class _PreferencePageState extends State<PreferencePage> {
  //4h selectgenres/memlih genres masih list kosng
  List<String> selectedGenres = [];
  //4h selectedLanguages dipilih english
  String selectedLanguages = 'english';

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          //4h artinya ketika di back maka pada signup password akan kosong
          widget.registrationData.password = '';
          context
              .bloc<PageBloc>()
              .add(GoToRegistrationPage(widget.registrationData));
          return;
        },
        child: Scaffold(
          body: Container(
            color: Colors.white,
            padding: EdgeInsets.symmetric(horizontal: defaultMargin),
            child: ListView(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    //4h untuk icon back arrow
                    Container(
                      height: 56,
                      margin: EdgeInsets.only(top: 20, bottom: 4),
                      child: GestureDetector(
                        onTap: () {
                          widget.registrationData.password = '';
                          context.bloc<PageBloc>().add(
                              GoToRegistrationPage(widget.registrationData));
                        },
                        child: Icon(Icons.arrow_back_ios),
                      ),
                    ),
                    Text(
                      'Select your\nfavorite genre',
                      style: blackTextFont.copyWith(fontSize: 20),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Wrap(
                      //untuk bagiankotakkotak
                      //jarak antara box kanan dan kiri
                      spacing: 24,
                      //run spacing jarak antara box atas dgn bawah
                      runSpacing: 24,
                      children: generateGenresWidget(context),
                    ),
                    SizedBox(
                      height: 24,
                    ),
                    Text(
                      'movie language\nyour preference',
                      style: blackTextFont.copyWith(fontSize: 20),
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Wrap(
                      //untuk bagian kotak kotak
                      spacing: 24,
                      runSpacing: 24,
                      children: generateLangWidget(context),
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Center(
                        child: FloatingActionButton(
                            elevation: 0,
                            backgroundColor: mainColor,
                            child: Icon(Icons.arrow_forward_ios),
                            onPressed: () {
                              if (selectedGenres.length != 4) {
                                Flushbar(
                                  duration: Duration(seconds: 4),
                                  flushbarPosition: FlushbarPosition.TOP,
                                  backgroundColor: Color(0xffff5c83),
                                  message: 'tolong pilih 4 genre',
                                );
                              } else {
                                //kalau semua sudah dipilih maka akan pindah ke page selanjutnya

                                widget.registrationData.selectedGenres =
                                    selectedGenres;
                                widget.registrationData.selectLang =
                                    selectedLanguages;
                                context.bloc<PageBloc>().add(
                                    GoToAccountConfirmationPage(
                                        widget.registrationData));
                              }
                            })),
                    SizedBox(
                      height: 50,
                    )
                    //diberi jarak karena beberapa hp yg tidak terlalu tinggi maka terlalu mepet dibawah FAB
                  ],
                )
              ],
            ),
          ),
        ));
  }

  List<Widget> generateGenresWidget(BuildContext context) {
    //4h 24 mungkin jarak antar kotak/element, intinya jarak antar kotak satu dgn yang lain
    //
    double width =
        (MediaQuery.of(context).size.width - 2 * defaultMargin - 24) / 2;

    return widget.genres
        .map((e) => SelectableBox(
              e,
              width: width,
              //4h isSelected/dipilihatautidak sebagai properti memiliki selectedGenres yg mengandung element
              isSelected: selectedGenres.contains(e),
              onTap: () {
                //ketika ditekan maka akan membuat fungsi onSelectedGenre aktif dan sesuai e/elemen
                onSelectedGenre(e);
              },
            ))
        .toList();
  }

  //4h buat fungsi onSelectedGenre/ketika menekan Genre
  void onSelectedGenre(String genre) {
    if (selectedGenres.contains(genre)) {
      setState(() {
        selectedGenres.remove(genre);
      });
    } else {
      if (selectedGenres.length < 4) {
        setState(() {
          selectedGenres.add(genre);
        });
      }
    }
  }

  List<Widget> generateLangWidget(BuildContext context) {
    double width =
        (MediaQuery.of(context).size.width - 2 * defaultMargin - 24) / 2;
    return widget.languages
        .map((e) => SelectableBox(
              e,
              width: width,
              isSelected: selectedLanguages == e,
              onTap: () {
                setState(() {
                  selectedLanguages = e;
                });
              },
            ))
        .toList();
  }
}
