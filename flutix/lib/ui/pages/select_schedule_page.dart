part of 'pages.dart';

class SelectShedulePage extends StatefulWidget {
  final MovieDetail movieDetail;
  SelectShedulePage(this.movieDetail);

  @override
  _SelectShedulePageState createState() => _SelectShedulePageState();
}

class _SelectShedulePageState extends State<SelectShedulePage> {
  List<DateTime> dates; //ini menampilkan list tglwaktu selama 7 hari kedepan
  DateTime selectedDate; //ini fungsinya untuk memilih tgl default/dimulai
  int selectedTime; //ini fungsinya untuk memilih jam/waktu secara default/dimulai
  // Theater selectedTheater; //ini fungsinya untuk memilih lokasi/tempat s
  bool isValid = false; // 6d apakah kita sudah memilih tgl/waktunya
  Theater selectedTheater;

  @override
  void initState() {
    super.initState();
    dates =
        List.generate(7, (index) => DateTime.now().add(Duration(days: index)));
    selectedDate = dates[0];
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          context.bloc<PageBloc>().add(GoToMovieDetailPage(widget.movieDetail));
          return;
        },
        child: Scaffold(
          body: Stack(
            children: [
              Container(
                color: accentColor1,
              ),
              SafeArea(
                  child: Container(
                color: Colors.white,
              )),
              ListView(
                children: [
                  //6d back icon
                  Row(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 20, left: defaultMargin),
                        padding: EdgeInsets.all(1),
                        child: GestureDetector(
                          onTap: () {
                            context
                                .bloc<PageBloc>()
                                .add(GoToMovieDetailPage(widget.movieDetail));
                          },
                          child: Icon(
                            Icons.arrow_back_ios,
                            color: Colors.black,
                          ),
                        ),
                      ),
                    ],
                  ),
                  //6d choose date
                  Container(
                    margin: EdgeInsets.fromLTRB(
                        defaultMargin, 24, defaultMargin, 16),
                    child: Text(
                      'Choose Date',
                      style: blackTextFont.copyWith(fontSize: 20),
                    ),
                  ),
                  //kotak hari/date
                  Container(
                    height: 90,
                    margin: EdgeInsets.only(bottom: 24),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: dates.length,
                        itemBuilder: (_, index) => Container(
                            margin: EdgeInsets.only(
                                left: (index == 0) ? defaultMargin : 0,
                                right: (index < dates.length - 1)
                                    ? 16
                                    : defaultMargin),
                            child: DateCard(
                              dates[index],
                              //6d isSelected/dipilih (default belum dipilih) diisi pilih tgl sama dgn dates/waktu sesuai index
                              isSelected: selectedDate == dates[index],
                              onTap: () {
                                setState(() {
                                  //6d ketika di tap maka selectedDate/pilih diinisialisasi tgl maka sesuai dgn dates/tgl index tsb
                                  selectedDate = dates[index];
                                });
                              },
                            ))),
                  ),
                  //pilih tgl theater
                  generateTimeTable(),
                  SizedBox(
                    height: 10,
                  ),
                  Align(
                    alignment: Alignment.topCenter,
                    child: BlocBuilder<UserBloc, UserState>(
                        builder: (_, userState) {
                      return FloatingActionButton(
                          elevation: 0,
                          backgroundColor:
                              (isValid) ? mainColor : Color(0xffe4e4e4),
                          child: Icon(
                            Icons.arrow_forward_ios,
                            color: isValid ? Colors.white : Color(0xffbebebe),
                          ),
                          onPressed: () {});
                    }),
                  )
                ],
              )
            ],
          ),
        ));
  }

  Column generateTimeTable() {
    //6d 10 itu menunjukan waktu dikali dua berarti 10 pagi dan malam, dan ada jedanyya dua jam dua jam
    List<int> schedule = List.generate(7, (index) => 10 + index * 2);
    List<Widget> widgets = [];

    //perulangan setiap bioskop yg ada dummy theater
    for (var theater in dummyTheater) {
      widgets.add(Container(
        margin: EdgeInsets.fromLTRB(defaultMargin, 0, defaultMargin, 16),
        child: Text(
          theater.name,
          style: blackTextFont.copyWith(fontSize: 20),
        ),
      ));
      widgets.add(Container(
        height: 50,
        margin: EdgeInsets.only(bottom: 20),
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: schedule.length,
            itemBuilder: (_, index) => Container(
                  margin: EdgeInsets.only(
                      left: (index == 0) ? defaultMargin : 0,
                      right: (index < schedule.length) ? 16 : defaultMargin),
                  child: SelectableBox(
                    '${schedule[index]}:00',
                    height: 50,
                    //6d isSelected/dpilih
                    //pilih theater sama dgn theater dan pilih waktu sama dgn schdule sesuai index
                    isSelected: selectedTheater == theater &&
                        selectedTime == schedule[index],
                    //6d yg akan dipilih adl waktu setlah saat ini, waktu setelah datetime.now
                    //jadi kalau pilih waktu yg lampau tidak bisa dipilih lagi
                    isEnabled: schedule[index] > DateTime.now().hour ||
                        selectedDate.day != DateTime.now().day,
                    onTap: () {
                      setState(() {
                        selectedTheater = theater;
                        selectedTime = schedule[index];
                        isValid = true;
                      });
                    },
                  ),
                )),
      ));
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: widgets,
    );
  }
}
