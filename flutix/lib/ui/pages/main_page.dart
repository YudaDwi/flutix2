part of 'pages.dart';

class MainPage extends StatefulWidget {
  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  int bottomNavBarIndex; //4b mengatur no navbarindex
  PageController pageController; //4b mengatur pageview nya

  @override
  void initState() {
    super.initState();
    bottomNavBarIndex = 0;
    pageController = PageController(initialPage: bottomNavBarIndex);
    //4b pagecontroller dimulai dari bottomNavBarIndex yg ke 0
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            color: accentColor1,
          ),
          SafeArea(
              child: Container(
            color: Color(0xfff6f7f9),
          )),
          PageView(
            controller: pageController,
            onPageChanged: (index) {
              setState(() {
                bottomNavBarIndex = index;
                //4b bila berubah maka setState/aturKeadaan sesuai dgn index saat ini
              });
            },
            children: [
              MoviePages(),
              Center(
                child: Text('lokasi dua'),
              )
            ],
          ),
          //4a jadi posisis customnavbar memang udah di bawah
          createCustomBottomNavBar(),
          //4a dan ini posisi ny akan di paskan dengan tengah nya itu
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              height: 46,
              width: 46,
              margin: EdgeInsets.only(bottom: 42),
              child: FloatingActionButton(
                  elevation: 0,
                  backgroundColor: accentColor2,
                  child: SizedBox(
                    height: 26,
                    width: 26,
                    child: Icon(
                      MdiIcons.walletPlus,
                      color: Colors.black.withOpacity(0.54),
                    ),
                  ),
                  onPressed: () {
                    context.bloc<UserBloc>().add(SignOut());
                    AuthServices.signOut();
                  }),
            ),
          )
        ],
      ),
    );
  }

  Widget createCustomBottomNavBar() => Align(
        alignment: Alignment.bottomCenter,
        child: ClipPath(
          clipper: BottomNavBarClipper(),
          child: Container(
            height: 70,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: BottomNavigationBar(
              onTap: (index) {
                setState(() {
                  //4b jadi bottomNavBarIndex di inisialisasi index saat ini/index ny BottomNavigationBar
                  bottomNavBarIndex = index;
                  //4b pageController akan melompat sesuai index saat ini
                  pageController.jumpToPage(index);
                });
              },
              items: [
                BottomNavigationBarItem(
                    label: 'movies baru',
                    icon: Container(
                      margin: EdgeInsets.only(bottom: 6),
                      height: 20,
                      child: Image.asset((bottomNavBarIndex == 0)
                          ? 'assets/ic_movie.png'
                          : 'assets/ic_movie_grey.png'),
                    )),
                BottomNavigationBarItem(
                    label: 'my tickets',
                    icon: Container(
                        margin: EdgeInsets.only(bottom: 6),
                        height: 20,
                        child: Image.asset((bottomNavBarIndex == 1)
                            ? 'assets/ic_tickets.png'
                            : 'assets/ic_tickets_grey.png')))
              ],
              elevation: 0,
              backgroundColor: Colors.transparent,
              selectedItemColor: Color(0xFF353535),
              currentIndex: bottomNavBarIndex,
            ),
          ),
        ),
      );
}

class BottomNavBarClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();

    path.lineTo(size.width / 2 - 28, 0);
    path.quadraticBezierTo(size.width / 2 - 28, 33, size.width / 2, 33);
    path.quadraticBezierTo(size.width / 2 + 28, 33, size.width / 2 + 28, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, size.height);
    path.lineTo(0, size.height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
