part of 'pages.dart';

class SignUpPage extends StatefulWidget {
  final RegistrationData registrationData;
  SignUpPage(this.registrationData);
  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController reTypePasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    //4e akan isi name sesuai dgn yg akan di registrationData
    //kalau sudah diisii string kosong/ kalau sudah diisi berarti sudah ada namnya
    nameController.text = widget.registrationData.name;
    emailController.text = widget.registrationData.email;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        context.bloc<PageBloc>().add(GoToSplashPage());
        return;
      },
      child: Scaffold(
        body: Container(
          color: Colors.white,
          padding: EdgeInsets.symmetric(horizontal: defaultMargin),
          child: ListView(
            children: [
              Column(
                children: [
                  //4d bagian atas
                  Container(
                    margin: EdgeInsets.only(top: 20, bottom: 22),
                    height: 56,
                    child: Stack(
                      children: [
                        Align(
                          alignment: Alignment.centerLeft,
                          child: GestureDetector(
                            onTap: () {
                              context.bloc<PageBloc>().add(GoToSplashPage());
                            },
                            child:
                                Icon(Icons.arrow_back_ios, color: Colors.black),
                          ),
                        ),
                        Center(
                          child: Text(
                            'Create New\nYour Account',
                            style: blackTextFont.copyWith(fontSize: 20),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //4e bgian profile picture
                  Container(
                    width: 90,
                    height: 104,
                    child: Stack(
                      children: [
                        Container(
                          height: 90,
                          width: 90,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  //4d menrut saya widget artinya memperbarui properti dan melakukan pembaruan
                                  //namun tetap dgn konfigurasi lama
                                  //dan widget itu berada di statefull widget
                                  image: (widget
                                              .registrationData.profileImage ==
                                          null)
                                      ? AssetImage('assets/user_pic.png')
                                      : FileImage(
                                          widget.registrationData.profileImage),
                                  fit: BoxFit.cover)),
                        ),
                        Align(
                          alignment: Alignment.bottomCenter,
                          child: GestureDetector(
                            onTap: () async {
                              if (widget.registrationData.profileImage ==
                                  null) {
                                widget.registrationData.profileImage =
                                    await getImage();
                              } else {
                                widget.registrationData.profileImage = null;
                              }
                            },
                            child: Container(
                              height: 28,
                              width: 28,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      image: AssetImage((widget.registrationData
                                                  .profileImage ==
                                              null)
                                          ? 'assets/btn_add_photo.png'
                                          : 'assets/btn_del_photo.png'))),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 36,
                  ),
                  TextField(
                    controller: nameController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'jeneng dowo',
                        hintText: 'jeneng dowo'),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    controller: emailController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'email mu lur',
                        hintText: 'email mu lur'),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'telik sandi',
                        hintText: 'nggawe sandi tenanan'),
                  ),
                  SizedBox(
                    height: 16,
                  ),
                  TextField(
                    controller: reTypePasswordController,
                    obscureText: true,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                        labelText: 'Re Type Password',
                        hintText: 'ulangi password mu lur'),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  FloatingActionButton(
                    //4e ! artinya bukan/tidak memenuhi syarat maka
                    //trim untuk buang whitespace di awal dan di akhir
                    onPressed: () {
                      if (!(nameController.text.trim() != '' &&
                          emailController.text.trim() != '' &&
                          passwordController.text.trim() != '' &&
                          reTypePasswordController.text.trim() != '')) {
                        Flushbar(
                          duration: Duration(seconds: 4),
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Color(0xffff5c83),
                          message: 'tolong diisi lur',
                        )..show(context);
                      } else if (passwordController.text !=
                          reTypePasswordController.text) {
                        Flushbar(
                          duration: Duration(seconds: 4),
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Color(0xffff5c83),
                          message: 'password mu gak cocok , CUK',
                        )..show(context);
                      } else if (passwordController.text.length < 6) {
                        Flushbar(
                          duration: Duration(seconds: 4),
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Color(0xffff5c83),
                          message: 'password kurang dari 6 karakter',
                        )..show(context);
                      } else if (!EmailValidator.validate(
                          emailController.text)) {
                        Flushbar(
                          duration: Duration(seconds: 4),
                          flushbarPosition: FlushbarPosition.TOP,
                          backgroundColor: Color(0xff5c83),
                          message: 'format email mu gak cocok , CUK',
                        );
                      } else {
                        //4e bila semua sudah mengisi maka akan mengarahka ke page selanjutnya
                        widget.registrationData.name = nameController.text;
                        widget.registrationData.email = emailController.text;
                        widget.registrationData.password =
                            passwordController.text;

                        context
                            .bloc<PageBloc>()
                            .add(GoToPreferencePage(widget.registrationData));
                      }
                    },
                    child: Icon(
                      Icons.arrow_forward_ios,
                      color: mainColor,
                    ),
                    elevation: 0,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
