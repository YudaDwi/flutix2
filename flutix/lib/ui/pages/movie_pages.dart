part of 'pages.dart';

class MoviePages extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        //note: header
        Container(
          decoration: BoxDecoration(
              color: accentColor1,
              borderRadius: BorderRadius.only(
                  bottomLeft: Radius.circular(20),
                  bottomRight: Radius.circular(20))),
          padding: EdgeInsets.fromLTRB(defaultMargin, 20, defaultMargin, 30),
          child: BlocBuilder<UserBloc, UserState>(builder: (_, userState) {
            if (userState is UserLoaded) {
              //4k bila userState sudah UserLoaded
              if (imageFileToUpload != null) {
                //4k then itu kalau sudah beres, dan si then ini mengembalikan uploadImage
                uploadImage(imageFileToUpload).then((downloadUrl) {
                  imageFileToUpload = null;
                  //4k update data user
                  context
                      .bloc<UserBloc>()
                      .add(UpdateData(profileImage: downloadUrl));
                });
              }
              return Row(
                children: [
                  //untuk profile picture
                  Container(
                    padding: EdgeInsets.all(5),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Color(0xff5f558b), width: 1)),
                    child: Stack(
                      children: [
                        SpinKitFadingCircle(
                          color: accentColor2,
                          size: 50,
                        ),
                        Container(
                          width: 50,
                          height: 50,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                image: (userState.user.profiilePicture == '')
                                    ? AssetImage('assets/user_pic.png')
                                    : NetworkImage(
                                        userState.user.profiilePicture),
                                fit: BoxFit.cover),
                          ),
                        )
                      ],
                    ),
                  ),
                  //untuk username dan price
                  SizedBox(
                    width: 16,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        //4c untuk jaga jga nama yg terlalu pnjang maka dibungkus dng sized box
                        // 50 lebar profilpic, 12 jarak border putih ke profile picture dan lebar border 1
                        // jarak border putih dg namanya
                        width: MediaQuery.of(context).size.width -
                            2 * defaultMargin -
                            50 -
                            12 -
                            16,
                        child: Text(
                          userState.user.name,
                          style: whiteTextFont.copyWith(fontSize: 18),
                          maxLines: 1,
                          overflow: TextOverflow.clip,
                        ),
                      ),
                      Text(
                        NumberFormat.currency(
                                locale: 'id_ID',
                                decimalDigits: 0,
                                symbol: 'IDR ')
                            .format(userState.user.balance),
                        style: yellowNumberFont.copyWith(
                            fontSize: 14, fontWeight: FontWeight.w400),
                      )
                    ],
                  )
                ],
              );
            } else {
              return SpinKitFadingCircle(
                color: accentColor2,
                size: 50,
              );
            }
          }),
        ),

        //5a buat now playing
        Container(
          margin: EdgeInsets.fromLTRB(defaultMargin, 30, defaultMargin, 12),
          child: Text(
            'Now Playing',
            style: blackTextFont.copyWith(fontSize: 16),
          ),
        ),
        //daftar film untuk now playing
        SizedBox(
          height: 140,
          child: BlocBuilder<MovieBloc, MovieState>(builder: (_, movieState) {
            if (movieState is MovieLoaded) {
              List<Movie> movies = movieState.movies.sublist(0, 10);
              return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: movies.length,
                  itemBuilder: (_, index) => Container(
                        margin: EdgeInsets.only(
                            //5b kalau index sama dgn yg pertama maka defaultMargin kalau tidak jarak 0
                            left: (index == 0) ? defaultMargin : 0,
                            //5b kalau index sama dengan panjangfilm - 1/ yg terakhir (10-1=9) maka defaultMargin kalau tidak 16
                            right: (index == movies.length - 1)
                                ? defaultMargin
                                : 16),
                        child: MovieCard(
                          movies[index],
                          onTap: () {
                            context
                                .bloc<PageBloc>()
                                .add(GoToMovieDetailPage(movies[index]));
                          },
                        ),
                      ));
            } else {
              return SpinKitFadingCircle(
                color: mainColor,
                size: 50,
              );
            }
          }),
        ),
        //5c text browse movie
        // Container(
        //   margin: EdgeInsets.fromLTRB(defaultMargin, 30, defaultMargin, 12),
        //   child: Text(
        //     'browse movie',
        //     style: blackTextFont.copyWith(
        //         fontSize: 18, fontWeight: FontWeight.bold),
        //   ),
        // ),
        //5c icon browse movie
        //menggunakan bloc builder karena data genre yg dipilih ada di data user

        // BlocBuilder<UserBloc, UserState>(builder: (_, userState) {
        //   //5c userloaded adl ketika data kita diload dari firebase
        //   if (userState is UserLoaded) {
        //     return Container(
        //       margin: EdgeInsets.symmetric(horizontal: defaultMargin),
        //       child: Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: List.generate(
        //             userState.user.selectedGenres.length,
        //             (index) =>
        //                 BrowseButton(userState.user.selectedGenres[index])),
        //       ),
        //     );
        //   } else {
        //     return SpinKitFadingCircle(
        //       size: 50,
        //       color: mainColor,
        //     );
        //   }
        // }),

        //5c teks comingsoon
        Container(
          margin: EdgeInsets.fromLTRB(defaultMargin, 30, defaultMargin, 12),
          child: Text(
            'Coming Soon',
            style: blackTextFont.copyWith(fontSize: 16),
          ),
        ),
        //5c daftar film coming soon
        SizedBox(
          height: 140,
          child: BlocBuilder<MovieBloc, MovieState>(builder: (_, movieState) {
            if (movieState is MovieLoaded) {
              //5c untuk belakng 10 film selanjutnya tidak diberi karena sudah di akhir , 10 hingga selesai
              List<Movie> movies = movieState.movies.sublist(10);
              return ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: movies.length,
                  itemBuilder: (_, index) => Container(
                        margin: EdgeInsets.only(
                            //5b kalau index sama dgn yg pertama maka defaultMargin kalau tidak jarak 0
                            left: (index == 0) ? defaultMargin : 0,
                            //5b kalau index sama dengan panjangfilm - 1/ yg terakhir (10-1=9) maka defaultMargin kalau tidak 16
                            right: (index == movies.length - 1)
                                ? defaultMargin
                                : 16),
                        child: ComingSoonCard(movies[index]),
                      ));
            } else {
              return SpinKitFadingCircle(
                color: mainColor,
                size: 50,
              );
            }
          }),
        ),

        //5d text get lucky day
        Container(
          margin: EdgeInsets.fromLTRB(defaultMargin, 30, defaultMargin, 12),
          child: Text(
            'get lucky day',
            style: blackTextFont.copyWith(fontSize: 16),
          ),
        ),
        Column(
          children: dummyPromo
              .map((e) => Padding(
                  padding:
                      EdgeInsets.fromLTRB(defaultMargin, 0, defaultMargin, 16),
                  child: PromoCard(e)))
              .toList(),
        ),
        SizedBox(
          height: 100,
        )
      ],
    );
  }
}
