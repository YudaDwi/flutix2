part of 'widgets.dart';

class DateCard extends StatelessWidget {
  final bool
      isSelected; //9c isSelected itu apakah ini sedang dipilih atau tidak
  final double width;
  final double height;
  final DateTime date; //9c menunjukan tgl yg mau ditampilkan
  final Function
      onTap; //9c menyimpan method ketika DateCard ini di tap maka akan keliatan fungsinya

  DateCard(this.date,
      {this.width = 70, this.height = 90, this.isSelected = false, this.onTap});
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          //9c ketika ontap tidak null/ditekan maka akan mengembalikan fungsi onTap parameter
          if (onTap != null) {
            onTap();
          }
        },
        child: Container(
          width: width,
          height: height,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              color: isSelected ? accentColor2 : Colors.transparent,
              border: Border.all(
                  color: isSelected ? Colors.transparent : Color(0xFFE4E4E4))),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                date.shortDayName,
                style: blackTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.w400),
              ),
              SizedBox(
                height: 6,
              ),
              Text(
                //6c day disini bawaan dari dart.core
                //day beda dgn shortdayname
                date.day.toString(),
                style: blackTextFont.copyWith(
                    fontSize: 16, fontWeight: FontWeight.w400),
              )
            ],
          ),
        ));
  }
}
