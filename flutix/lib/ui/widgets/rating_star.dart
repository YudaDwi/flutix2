part of 'widgets.dart';

class RatingStar extends StatelessWidget {
  final double voteAverage; //5b voteAverage itu adalah angka rating
  final double starSize; //ukuran bintang/rating
  final double fontSize;
  final Color color;
  final MainAxisAlignment alignment;

  RatingStar(
      {this.voteAverage = 0,
      this.starSize = 20,
      this.fontSize = 12,
      this.color,
      this.alignment = MainAxisAlignment.start});

  @override
  Widget build(BuildContext context) {
    int n = (voteAverage / 2).round();
    //5b round itu artinya pembulatan
    // / 2
    // ~/ lambang tilde backslice artinya  si A dibagi B lalu dipotong/di truncate dibuang komanya
    List<Widget> widgets = List.generate(
        5,
        (index) => Icon(
              index < n ? MdiIcons.star : MdiIcons.starOutline,
              color: accentColor2,
              size: starSize,
            ));
    widgets.add(SizedBox(
      width: 3,
    ));
    widgets.add(Text(
      '$voteAverage/10',
      style:
          whiteNumberFont.copyWith(fontSize: 13, fontWeight: FontWeight.w300),
    ));

    return Row(
      children: widgets,
    );
  }
}
