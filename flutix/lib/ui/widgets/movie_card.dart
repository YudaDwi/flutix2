part of 'widgets.dart';

class MovieCard extends StatelessWidget {
  //5b movie yg akan ditampilkan ini berasal dari Movie model
  final Movie movie;
  final Function onTap;
  //5b ontap yg akan muncul ketika ditekan

  //buat 2 parameter
  MovieCard(this.movie, {this.onTap});

  @override
  Widget build(BuildContext context) {
    //6b jadi fungsi on tap ini agar kita tidak perlu menuliskan properti onTap di MovieCard
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        height: 140,
        width: 100,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(8),
            image: DecorationImage(
                image: NetworkImage(imageBaseUrl + 'w780' + movie.backdropPath),
                fit: BoxFit.cover)),
        //5b container kedua ini untuk gradient
        child: Container(
          height: 140,
          width: 100,
          padding: EdgeInsets.all(16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
              gradient: LinearGradient(
                  begin: Alignment.bottomCenter,
                  end: Alignment.topCenter,
                  colors: [
                    Colors.black.withOpacity(0.61),
                    Colors.black.withOpacity(0)
                  ])),
          // child: Column(
          //   mainAxisAlignment: MainAxisAlignment.end,
          //   crossAxisAlignment: CrossAxisAlignment.start,
          //   children: [
          //     Text(
          //       movie.title,
          //       style: whiteTextFont,
          //       maxLines: 2,
          //       overflow: TextOverflow.ellipsis,
          //     ),
          //     RatingStar(
          //       voteAverage: movie.voteAverage,
          //     )
          //   ],
          // ),
        ),
      ),
    );
  }
}
