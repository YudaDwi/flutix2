import 'package:flutix/models/models.dart';
import 'package:flutix/shared/shared.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutix/extensions/extensions.dart';

part 'rating_star.dart';
part 'movie_card.dart';
part 'selectable_box.dart';
part 'browse_button.dart';
part 'coming_soon.dart';
part 'promo_card.dart';
part 'credit_card.dart';
part 'date_card.dart';
