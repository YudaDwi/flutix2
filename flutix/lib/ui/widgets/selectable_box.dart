part of 'widgets.dart';

class SelectableBox extends StatelessWidget {
  final bool isSelected; //4g menunjukan apakah sedng dipilih/tidak
  final bool isEnabled; //menunjukan sedang di enabled/dinyalakan atau tidk
  final double width;
  final double height;
  final String text;
  final Function onTap; //4g menunjukan apa yg terjadi ketika di tap/tekan
  final TextStyle textStyle;

  SelectableBox(this.text,
      {this.isSelected = false,
      this.isEnabled = true,
      this.width = 144,
      this.height = 60,
      this.onTap,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (onTap != null) {
          onTap();
        }
      },
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            //4g kalau tidak dinylakan maka berwarna putih
            //kalau tidak isSelected/dipilih accentcolor2 kalau tidak colors.tranparan
            color: (!isEnabled)
                ? Color(0xffe3e4e4)
                : isSelected
                    ? accentColor2
                    : Colors.transparent,
            border: Border.all(
                color: (!isEnabled)
                    ? Color(0xffe4e4e4)
                    : isSelected
                        ? Colors.transparent
                        : Color(0xffe4e4e4))),
        child: Center(
          child: Text(
            //4g arti dari ?? adl pengecekan
            text ?? 'none',
            style: (textStyle ?? blackTextFont)
                .copyWith(fontSize: 16, fontWeight: FontWeight.w400),
          ),
        ),
      ),
    );
  }
}
