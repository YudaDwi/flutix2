part of 'models.dart';

//1dA disini dimulai buat apa yg dibutuhkan ketika dia mendaftar login
class User extends Equatable {
  final String id;
  final String email;
  final String name;
  final String profiilePicture;
  final List<String> selectedGenres;
  final String selectedLanguage;
  final int balance;

  User(this.id, this.email,
      {this.name,
      this.profiilePicture,
      this.selectedGenres,
      this.selectedLanguage,
      this.balance});

  //4k buat satu buah method copyWith fungsinya untuk mencopy dari si user hanya dengan beberapa properti propertinya
  //name nya diambil name yg baru , tapi kalau name yg baru null/?? maka dia akan mengambil name saat ini
  ////profile picture diambil profile picture yg baru kalau null/?? maka dia akan mengambil profilePicture saat ini
  //untuk yg tidak bisa berubah seperi id email selectedgenres selectedLanguage
  User copyWith({String name, String profilePicture, int balance}) =>
      User(this.id, this.email,
          name: name ?? this.name,
          profiilePicture: profiilePicture ?? this.profiilePicture,
          balance: balance ?? this.balance,
          selectedGenres: selectedGenres,
          selectedLanguage: selectedLanguage);

  //membuat fungsi toString yg urut dari id ke name dan email
  //belum paham override
  @override
  String toString() {
    return '[$id] - $name,$email';
  }

  @override
  List<Object> get props => [
        id,
        email,
        name,
        profiilePicture,
        selectedGenres,
        selectedLanguage,
        balance
      ];
}
