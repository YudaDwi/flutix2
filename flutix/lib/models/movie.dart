part of 'models.dart';

class Movie extends Equatable {
  final int id; //5a id movie nya
  final String title; //5a judul movie nya
  final double voteAverage; //5a rating nya
  final String overView; //5a deskripsi
  final String posterPath; //5a path utuk gambar posternya
  final String backdropPath; //5a path untuk backdrop path nya

  Movie(
      {@required this.id,
      @required this.title,
      @required this.voteAverage,
      @required this.overView,
      @required this.posterPath,
      @required this.backdropPath});

  //5a buat factory method
  factory Movie.fromJson(Map<String, dynamic> json) => Movie(
      id: json['id'],
      title: json['title'],
      overView: json['overview'],
      voteAverage: (json['vote_average'] as num).toDouble(),
      posterPath: json['poster_path'],
      backdropPath: json['backdrop_path']);

  @override
  List<Object> get props =>
      [id, title, voteAverage, overView, posterPath, backdropPath];
}
