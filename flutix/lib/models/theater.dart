part of 'models.dart';

class Theater extends Equatable {
  final String name;
  Theater(this.name);
  @override
  List<Object> get props => [name];
}

List<Theater> dummyTheater = [
  Theater('CGV Paskal Hyper Square'),
  Theater('PCC Cinemax Ponorogo'),
  Theater('XXI Cihampelas Walk'),
  Theater('XXI Bandung Trade Center')
];
