part of 'models.dart';

class RegistrationData {
  String name;
  String email;
  String password;
  List<String> selectedGenres;
  String selectLang;
  //4d untuk File karena akan diambil fotonya lewat hp
  //dan harus import dari io
  File profileImage;

  RegistrationData(
      {this.name = '',
      this.email = '',
      this.password = '',
      this.selectedGenres = const [],
      this.selectLang = '',
      this.profileImage});
}
