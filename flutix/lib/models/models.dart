import 'package:equatable/equatable.dart';
import 'dart:io';
import 'package:flutter/material.dart';

part 'user.dart';
part 'registration_data.dart';
part 'movie.dart';
part 'promo.dart';
part 'movie_details.dart';
part 'credit.dart';
part 'theater.dart';
