part of 'models.dart';

//6a karena beberapa atribut dimiliki oleh Movie(model)
class MovieDetail extends Movie {
  final List<String> genres;
  final String language;

  //6a super yaitu pemanggilan constructor parent nya yaitu Movie(model)
  MovieDetail(Movie movie, {this.genres, this.language})
      : super(
            id: movie.id,
            title: movie.title,
            voteAverage: movie.voteAverage,
            overView: movie.overView,
            posterPath: movie.posterPath,
            backdropPath: movie.backdropPath);

  //6a buat get er yg akan mengembalikan string , dan yg akan dikembalikan itu adl genre dan language nya
  //lumayan ribet gek gak paham
  String get genresAndLanguage {
    String s = '';

    for (var genre in genres) {
      s += genre + (genre != genres.last ? ' ' : '');
    }
    return '$s - $language';
  }

  //5a ini dari equatable nya movie
  @override
  List<Object> get props => super.props + [genres, language];
}
