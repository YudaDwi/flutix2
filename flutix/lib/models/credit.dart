part of 'models.dart';

class Credit extends Equatable {
  final String name; //6a ini nama dari aktornya
  final String profilePath; //6a ini foto dari aktornya

  Credit({this.name, this.profilePath});

  @override
  List<Object> get props => [name, profilePath];
}
