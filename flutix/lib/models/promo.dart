part of 'models.dart';

class Promo extends Equatable {
  final String title;
  final String subtitle;
  final int discount;

  Promo(
      {@required this.title, @required this.subtitle, @required this.discount});

  @override
  List<Object> get props => [title, subtitle, discount];
}

List<Promo> dummyPromo = [
  Promo(
      title: 'Student holiday',
      subtitle: 'maximal only for two people',
      discount: 50),
  Promo(
      title: 'Family club', subtitle: 'minimum for three member', discount: 70),
  Promo(title: 'subcription promo', subtitle: 'min one year', discount: 40)
];
